//
//  GameScene.swift
//  PestControl
//
//  Created by Grecia Brizuela on 11/13/23.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var worldNode: SKNode!
    var backgroundLayer: TileMapLayer!
    
    func createScenery() -> TileMapLayer {
        return TileMapLayer(atlasName: "scenery", tileSize: CGSize(width: 32, height: 32), tileCodes: ["xxxxxxxxxxxxxxxxxx",
             "xoooooooooooooooox",
             "xoooooooooooooooox",
             "xoooooooooooooooox",
             "xoooooooooooooooox",
             "xoooooooooxoooooox",
             "xoooooooooxoooooox",
             "xoooooxxxxxoooooox",
             "xoooooxoooooooooox",
             "xxxxxxxxxxxxxxxxxx"])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    override init(size: CGSize) {
        super.init(size: size)
    }
    
    override func didMove(to view: SKView) {
        createWorld()
    }
    
    func createWorld() {
        backgroundLayer = createScenery()
        
        worldNode = SKNode()
        worldNode.addChild(backgroundLayer)
        addChild(worldNode)
        
        anchorPoint = CGPointMake(0.5, 0.5)
        worldNode.position =
        CGPointMake(-backgroundLayer.layerSize.width / 2,               -backgroundLayer.layerSize.height / 2)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
