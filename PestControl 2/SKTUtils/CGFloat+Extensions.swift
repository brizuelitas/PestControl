/*
 * Copyright (c) 2013-2014 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import CoreGraphics

/** El valor de π como CGFloat */
let π = CGFloat(Double.pi)

public extension CGFloat {
    /**
     * Convierte un ángulo en grados a radianes.
     */
    func degreesToRadians() -> CGFloat {
        return π * self / 180.0
    }

    /**
     * Convierte un ángulo en radianes a grados.
     */
    func radiansToDegrees() -> CGFloat {
        return self * 180.0 / π
    }

    /**
     * Asegura que el valor flotante permanezca entre los valores dados, inclusivo.
     */
    func clamped(v1: CGFloat, _ v2: CGFloat) -> CGFloat {
        let min = v1 < v2 ? v1 : v2
        let max = v1 > v2 ? v1 : v2
        return self < min ? min : (self > max ? max : self)
    }

    /**
     * Asegura que el valor flotante permanezca entre los valores dados, inclusivo.
     */
    mutating func clamp(v1: CGFloat, _ v2: CGFloat) -> CGFloat {
        self = clamped(v1: v1, v2)
        return self
    }

    /**
     * Devuelve 1.0 si un valor flotante es positivo; -1.0 si es negativo.
     */
    func sign() -> CGFloat {
        return (self >= 0.0) ? 1.0 : -1.0
    }

    /**
     * Devuelve un número flotante aleatorio entre 0.0 y 1.0, inclusivo.
     */
    static func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 4294967296)
    }

    /**
     * Devuelve un número flotante aleatorio en el rango min...max, inclusivo.
     */
    static func random(min: CGFloat, max: CGFloat) -> CGFloat {
        assert(min < max)
        return CGFloat.random() * (max - min) + min
    }

    /**
     * Devuelve aleatoriamente 1.0 o -1.0.
     */
    static func randomSign() -> CGFloat {
        return (arc4random_uniform(2) == 0) ? 1.0 : -1.0
    }
}

/**
 * Devuelve el ángulo más corto entre dos ángulos. El resultado siempre está entre
 * -π y π.
 */
func shortestAngleBetween(angle1: CGFloat, angle2: CGFloat) -> CGFloat {
    let twoπ = π * 2.0
    var angle = (angle2 - angle1).truncatingRemainder(dividingBy: twoπ)
    if (angle >= π) {
        angle = angle - twoπ
    }
    if (angle <= -π) {
        angle = angle + twoπ
    }
    return angle
}

