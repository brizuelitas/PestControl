/*
 * Copyright (c) 2013-2014 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import CoreGraphics

public extension CGPoint {
    /**
     * Crea un nuevo CGPoint a partir de un CGVector.
     */
    init(vector: CGVector) {
        self.init(x: vector.dx, y: vector.dy)
    }

    /**
     * Dado un ángulo en radianes, crea un vector de longitud 1.0 y devuelve el
     * resultado como un nuevo CGPoint. Se asume que un ángulo de 0 apunta hacia la derecha.
     */
    init(angle: CGFloat) {
        self.init(x: cos(angle), y: sin(angle))
    }

    /**
     * Suma (dx, dy) al punto.
     */
    mutating func offset(dx: CGFloat, dy: CGFloat) -> CGPoint {
        x += dx
        y += dy
        return self
    }

    /**
     * Devuelve la longitud (magnitud) del vector descrito por el CGPoint.
     */
    func length() -> CGFloat {
        return sqrt(x * x + y * y)
    }

    /**
     * Devuelve la longitud al cuadrado del vector descrito por el CGPoint.
     */
    func lengthSquared() -> CGFloat {
        return x * x + y * y
    }

    /**
     * Normaliza el vector descrito por el CGPoint a longitud 1.0 y devuelve el
     * resultado como un nuevo CGPoint.
     */
    func normalized() -> CGPoint {
        let len = length()
        return len > 0 ? self / len : .zero
    }

    /**
     * Normaliza el vector descrito por el CGPoint a longitud 1.0.
     */
    mutating func normalize() -> CGPoint {
        self = normalized()
        return self
    }

    /**
     * Calcula la distancia entre dos CGPoints. ¡Pitágoras!
     */
    func distanceTo(point: CGPoint) -> CGFloat {
        return (self - point).length()
    }

    /**
     * Devuelve el ángulo en radianes del vector descrito por el CGPoint.
     * El rango del ángulo es de -π a π; un ángulo de 0 apunta hacia la derecha.
     */
    var angle: CGFloat {
        return atan2(y, x)
    }
}

/**
 * Suma dos valores CGPoint y devuelve el resultado como un nuevo CGPoint.
 */
public func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

/**
 * Incrementa un CGPoint con el valor de otro.
 */
public func += (left: inout CGPoint, right: CGPoint) {
    left = left + right
}

/**
 * Suma un CGVector a este CGPoint y devuelve el resultado como un nuevo CGPoint.
 */
public func + (left: CGPoint, right: CGVector) -> CGPoint {
    return CGPoint(x: left.x + right.dx, y: left.y + right.dy)
}

/**
 * Incrementa un CGPoint con el valor de un CGVector.
 */
public func += (left: inout CGPoint, right: CGVector) {
    left = left + right
}

/**
 * Resta dos valores CGPoint y devuelve el resultado como un nuevo CGPoint.
 */
public func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

/**
 * Decrementa un CGPoint con el valor de otro.
 */
public func -= (left: inout CGPoint, right: CGPoint) {
    left = left - right
}

/**
 * Resta un CGVector de un CGPoint y devuelve el resultado como un nuevo CGPoint.
 */
public func - (left: CGPoint, right: CGVector) -> CGPoint {
    return CGPoint(x: left.x - right.dx, y: left.y - right.dy)
}

/**
 * Decrementa un CGPoint con el valor de un CGVector.
 */
public func -= (left: inout CGPoint, right: CGVector) {
    left = left - right
}

/**
 * Multiplica dos valores CGPoint y devuelve el resultado como un nuevo CGPoint.
 */
public func * (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x * right.x, y: left.y * right.y)
}

/**
 * Multiplica un CGPoint con otro.
 */
public func *= (left: inout CGPoint, right: CGPoint) {
    left = left * right
}

/**
 * Multiplica los campos x e y de un CGPoint con el mismo valor escalar y
 * devuelve el resultado como un nuevo CGPoint.
 */
public func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

/**
 * Multiplica los campos x e y de un CGPoint con el mismo valor escalar.
 */
public func *= (left: inout CGPoint, scalar: CGFloat) {
    left = left * scalar
}

/**
 * Multiplica un CGPoint con un CGVector y devuelve el resultado como un nuevo CGPoint.
 */
public func * (left: CGPoint, right: CGVector) -> CGPoint {
    return CGPoint(x: left.x * right.dx, y: left.y * right.dy)
}

/**
 * Multiplica un CGPoint con un CGVector.
 */
public func *= (left: inout CGPoint, right: CGVector) {
    left = left * right
}

/**
 * Divide dos valores CGPoint y devuelve el resultado como un nuevo CGPoint.
 */
public func / (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x / right.x, y: left.y / right.y)
}

/**
 * Divide un CGPoint por otro.
 */
public func /= (left: inout CGPoint, right: CGPoint) {
    left = left / right
}

/**
 * Divide los campos x e y de un CGPoint por el mismo valor escalar y devuelve
 * el resultado como un nuevo CGPoint.
 */
public func / (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

/**
 * Divide los campos x e y de un CGPoint por el mismo valor escalar.
 */
public func /= (left: inout CGPoint, scalar: CGFloat) {
    left = left / scalar
}

/**
 * Realiza una interpolación lineal entre dos valores CGPoint.
 */
public func lerp(start: CGPoint, end: CGPoint, t: CGFloat) -> CGPoint {
    return CGPoint(x: start.x + (end.x - start.x) * t, y: start.y + (end.y - start.y) * t)
}

