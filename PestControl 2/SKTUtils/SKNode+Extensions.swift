/*
 * Copyright (c) 2013-2014 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import SpriteKit

public extension SKNode {
    
    /** Te permite tratar la escala del nodo como un valor CGPoint. */
    var scaleAsPoint: CGPoint {
        get {
            return CGPoint(x: xScale, y: yScale)
        }
        set {
            xScale = newValue.x
            yScale = newValue.y
        }
    }

    /**
     * Ejecuta una acción en el nodo que realiza un cierre o función después
     * de un tiempo determinado.
     */
    func afterDelay(delay: TimeInterval, runBlock block: @escaping () -> Void) {
        run(SKAction.sequence([SKAction.wait(forDuration: delay), SKAction.run(block)]))
    }

    /**
     * Coloca este nodo en la parte delantera de su padre.
     */
    func bringToFront() {
        if let parent = self.parent {
            removeFromParent()
            parent.addChild(self)
        }
    }

    /**
     * Orienta el nodo en la dirección en la que se está moviendo, interpolando su
     * ángulo de rotación. Esto asume que a 0 grados, el nodo está mirando hacia arriba.
     *
     * - Parameter velocity: La velocidad y dirección actual del nodo. Puedes obtener
     *        esto desde node.physicsBody.velocity.
     * - Parameter rate: Qué tan rápido gira el nodo. Debe tener un valor entre 0.0 y
     *        1.0, donde un valor más pequeño significa más lento; 1.0 es instantáneo.
     */
    func rotateToVelocity(velocity: CGVector, rate: CGFloat) {
        // Determina cuál debería ser el ángulo de rotación del nodo en función de la
        // velocidad actual de su cuerpo físico. Esto asume que a 0 grados el nodo está
        // mirando hacia arriba, no hacia la derecha, por lo que para compensar, restamos
        // π/4 (90 grados) del ángulo calculado.
        let newAngle = atan2(velocity.dy, velocity.dx) - π/2

        // Esto siempre hace que el nodo rote en la distancia más corta posible.
        // Dado que el rango de atan2() es de -180 a 180 grados, una rotación de,
        // -170 a -190 de lo contrario sería de -170 a 170, lo que hace que el nodo
        // gire en la dirección (y la distancia) incorrecta. Ajustamos el ángulo para
        // que vaya de 190 a 170 en su lugar, que es equivalente a -170 a -190.
        if newAngle - zRotation > π {
            zRotation += π * 2.0
        } else if zRotation - newAngle > π {
            zRotation -= π * 2.0
        }

        // Usa la "interpolación exponencial estándar" para cambiar lentamente zRotation
        // hacia el nuevo ángulo. Cuanto mayor sea el valor de rate, más rápido se moverá.
        zRotation += (newAngle - zRotation) * rate
    }
}
