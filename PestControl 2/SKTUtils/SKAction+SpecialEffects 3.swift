/*
 * Copyright (c) 2013-2014 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import SpriteKit

public extension SKAction {
    /**
     * Crea una animación de sacudida de pantalla.
     *
     * - Parameter node: El nodo que se sacudirá. No se puede aplicar este efecto a una SKScene.
     * - Parameter amount: El vector por el cual se desplaza el nodo.
     * - Parameter oscillations: El número de oscilaciones; 10 es un buen valor.
     * - Parameter duration: Cuánto dura el efecto. Menos es mejor.
     */
    class func screenShakeWithNode(node: SKNode, amount: CGPoint, oscillations: Int, duration: TimeInterval) -> SKAction {
        let oldPosition = node.position
        let newPosition = oldPosition + amount
        
        let effect = SKTMoveEffect(node: node, duration: duration, startPosition: newPosition, endPosition: oldPosition)
        effect.timingFunction = SKTCreateShakeFunction(oscillations: oscillations)

        return SKAction.actionWithEffect(effect: effect)
    }

    /**
     * Crea una animación de rotación de pantalla.
     *
     * - Parameter node: Normalmente deseas aplicar este efecto a un nodo pivote centrado en la escena. No se puede aplicar el efecto a una SKScene.
     * - Parameter angle: El ángulo en radianes.
     * - Parameter oscillations: El número de oscilaciones; 10 es un buen valor.
     * - Parameter duration: Cuánto dura el efecto. Menos es mejor.
     */
    class func screenRotateWithNode(node: SKNode, angle: CGFloat, oscillations: Int, duration: TimeInterval) -> SKAction {
        let oldAngle = node.zRotation
        let newAngle = oldAngle + angle
        
        let effect = SKTRotateEffect(node: node, duration: duration, startAngle: newAngle, endAngle: oldAngle)
        effect.timingFunction = SKTCreateShakeFunction(oscillations: oscillations)

        return SKAction.actionWithEffect(effect: effect)
    }

    /**
     * Crea una animación de zoom de pantalla.
     *
     * - Parameter node: Normalmente deseas aplicar este efecto a un nodo pivote centrado en la escena. No se puede aplicar el efecto a una SKScene.
     * - Parameter amount: Cuánto escalar el nodo en las direcciones X e Y.
     * - Parameter oscillations: El número de oscilaciones; 10 es un buen valor.
     * - Parameter duration: Cuánto dura el efecto. Menos es mejor.
     */
    class func screenZoomWithNode(node: SKNode, amount: CGPoint, oscillations: Int, duration: TimeInterval) -> SKAction {
        let oldScale = CGPoint(x: node.xScale, y: node.yScale)
        let newScale = oldScale * amount
        
        let effect = SKTScaleEffect(node: node, duration: duration, startScale: newScale, endScale: oldScale)
        effect.timingFunction = SKTCreateShakeFunction(oscillations: oscillations)

        return SKAction.actionWithEffect(effect: effect)
    }

    /**
     * Hace que el fondo de la escena parpadee durante duration segundos.
     */
    class func colorGlitchWithScene(scene: SKScene, originalColor: SKColor, duration: TimeInterval) -> SKAction {
        return SKAction.customAction(withDuration: duration) { (node, elapsedTime) in
            if elapsedTime < CGFloat(duration) {
                scene.backgroundColor = SKColorWithRGB(r: Int.random(in: 0...255), g: Int.random(in: 0...255), b: Int.random(in: 0...255))
            } else {
                scene.backgroundColor = originalColor
            }
        }
    }
}


