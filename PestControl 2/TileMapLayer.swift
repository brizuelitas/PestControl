//
//  TileMapLayer.swift
//  PestControl
//
//  Created by Grecia Brizuela on 11/15/23.
//

import Foundation
import SpriteKit

class TileMapLayer: SKNode {
    
    let tileSize: CGSize
    var atlas: SKTextureAtlas?
    let gridSize: CGSize
    let layerSize: CGSize
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    init(tileSize: CGSize, gridSize: CGSize, layerSize: CGSize? = nil) {
        self.tileSize = tileSize
        self.gridSize = gridSize
        if layerSize != nil {
            self.layerSize = layerSize!
        } else {
            self.layerSize = CGSize(width: tileSize.width * gridSize.width, height: tileSize.height * gridSize.height)
        }
        
        super.init()
    }
    
    func nodeForCode(tileCode: Character) -> SKNode? {
        
        if atlas == nil {
            return nil
        }
        
        var tile: SKNode?
        switch tileCode {
        case "x": tile = SKSpriteNode(texture: atlas!.textureNamed("wall"))
            
        case "o": tile = SKSpriteNode(texture: atlas!.textureNamed("grass1"))
            
        default:
            print("Unknown tile code \(tileCode)")
        }
        
        if let sprite = tile as? SKSpriteNode {
            sprite.blendMode = .replace
            sprite.texture?.filteringMode = .nearest
        }
        return tile
    }
    
    convenience init(atlasName: String, tileSize: CGSize, tileCodes: [String]) {
        self.init(tileSize: tileSize,
                  gridSize: CGSize(width: tileCodes[0].count,
                                   height: tileCodes.count))
        atlas = SKTextureAtlas(named: atlasName)
        
        for row in 0..<tileCodes.count {
            let line = tileCodes[row]
            for (col, code) in line.enumerated() {
                if let tile = nodeForCode(tileCode: code) {
                    tile.position = positionForRow(row: row, col: col)
                    addChild(tile)
                }
            }
        }
    }
    
    func positionForRow(row: Int, col: Int) -> CGPoint {
        let x = CGFloat(col) * tileSize.width + tileSize.width / 2
        let y = CGFloat(row) * tileSize.height + tileSize.height / 2
        return CGPoint(x: x, y: layerSize.height - y)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
