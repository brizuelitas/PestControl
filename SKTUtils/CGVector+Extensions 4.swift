/*
 * Copyright (c) 2013-2014 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import CoreGraphics
import SpriteKit

extension CGVector {
    /**
     * Crea un nuevo CGVector dado un CGPoint.
     */
    init(point: CGPoint) {
        self.init(dx: point.x, dy: point.y)
    }

    /**
     * Dado un ángulo en radianes, crea un vector de longitud 1.0 y devuelve el resultado como un nuevo CGVector.
     * Se asume que un ángulo de 0 apunta hacia la derecha.
     */
    init(angle: CGFloat) {
        self.init(dx: cos(angle), dy: sin(angle))
    }

    /**
     * Suma (dx, dy) al vector.
     */
    mutating func offset(dx: CGFloat, dy: CGFloat) -> CGVector {
        self.dx += dx
        self.dy += dy
        return self
    }

    /**
     * Devuelve la longitud (magnitud) del vector descrito por el CGVector.
     */
    func length() -> CGFloat {
        return sqrt(dx * dx + dy * dy)
    }

    /**
     * Devuelve la longitud al cuadrado del vector descrito por el CGVector.
     */
    func lengthSquared() -> CGFloat {
        return dx * dx + dy * dy
    }

    /**
     * Normaliza el vector descrito por el CGVector a longitud 1.0 y devuelve el resultado como un nuevo CGVector.
     */
    func normalized() -> CGVector {
        let len = length()
        return len > 0 ? self / len : CGVector.zero
    }

    /**
     * Normaliza el vector descrito por el CGVector a longitud 1.0.
     */
    mutating func normalize() -> CGVector {
        self = normalized()
        return self
    }

    /**
     * Calcula la distancia entre dos CGVectors. ¡Teorema de Pitágoras!
     */
    func distanceTo(vector: CGVector) -> CGFloat {
        return (self - vector).length()
    }

    /**
     * Devuelve el ángulo en radianes del vector descrito por el CGVector.
     * El rango del ángulo es de -π a π; un ángulo de 0 apunta hacia la derecha.
     */
    var angle: CGFloat {
        return atan2(dy, dx)
    }
}

/**
 * Suma dos valores de CGVector y devuelve el resultado como un nuevo CGVector.
 */
func + (left: CGVector, right: CGVector) -> CGVector {
    return CGVector(dx: left.dx + right.dx, dy: left.dy + right.dy)
}

/**
 * Incrementa un CGVector con el valor de otro.
 */
func += (left: inout CGVector, right: CGVector) {
    left = left + right
}

/**
 * Resta dos valores de CGVector y devuelve el resultado como un nuevo CGVector.
 */
func - (left: CGVector, right: CGVector) -> CGVector {
    return CGVector(dx: left.dx - right.dx, dy: left.dy - right.dy)
}

/**
 * Decrementa un CGVector con el valor de otro.
 */
func -= (left: inout CGVector, right: CGVector) {
    left = left - right
}

/**
 * Multiplica dos valores de CGVector y devuelve el resultado como un nuevo CGVector.
 */
func * (left: CGVector, right: CGVector) -> CGVector {
    return CGVector(dx: left.dx * right.dx, dy: left.dy * right.dy)
}

/**
 * Multiplica un CGVector con otro.
 */
func *= (left: inout CGVector, right: CGVector) {
    left = left * right
}

/**
 * Multiplica los campos x e y de un CGVector con el mismo valor escalar y devuelve el resultado como un nuevo CGVector.
 */
func * (vector: CGVector, scalar: CGFloat) -> CGVector {
    return CGVector(dx: vector.dx * scalar, dy: vector.dy * scalar)
}

/**
 * Multiplica los campos x e y de un CGVector con el mismo valor escalar.
 */
func *= (vector: inout CGVector, scalar: CGFloat) {
    vector = vector * scalar
}

/**
 * Divide dos valores de CGVector y devuelve el resultado como un nuevo CGVector.
 */
func / (left: CGVector, right: CGVector) -> CGVector {
    return CGVector(dx: left.dx / right.dx, dy: left.dy / right.dy)
}

/**
 * Divide un CGVector por otro.
 */
func /= (left: inout CGVector, right: CGVector) {
    left = left / right
}

/**
 * Divide los campos dx y dy de un CGVector por el mismo valor escalar y devuelve el resultado como un nuevo CGVector.
 */
func / (vector: CGVector, scalar: CGFloat) -> CGVector {
    return CGVector(dx: vector.dx / scalar, dy: vector.dy / scalar)
}

/**
 * Divide los campos dx y dy de un CGVector por el mismo valor escalar.
 */
func /= (vector: inout CGVector, scalar: CGFloat) {
    vector = vector / scalar
}

/**
 * Realiza una interpolación lineal entre dos valores de CGVector.
 */
func lerp(start: CGVector, end: CGVector, t: CGFloat) -> CGVector {
    return CGVector(dx: start.dx + (end.dx - start.dx) * t, dy: start.dy + (end.dy - start.dy) * t)
}
