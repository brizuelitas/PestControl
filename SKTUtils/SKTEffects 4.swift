/*
 * Copyright (c) 2013-2014 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import SpriteKit

/**
 * Permite realizar acciones con funciones de temporización personalizadas.
 *
 * Desafortunadamente, SKAction no tiene un concepto de función de temporización, por lo que
 * necesitamos replicar las acciones utilizando subclases de SKTEffect.
 */
public class SKTEffect {
    unowned var node: SKNode
    var duration: TimeInterval
    public var timingFunction: ((CGFloat) -> CGFloat)?

    public init(node: SKNode, duration: TimeInterval) {
        self.node = node
        self.duration = duration
        timingFunction = SKTTimingFunctionLinear
    }

    public func update(t: CGFloat) {
        // Las subclases implementan esto
    }
}

/**
 * Mueve un nodo desde su posición actual a una nueva posición.
 */
public class SKTMoveEffect: SKTEffect {
    var startPosition: CGPoint
    var delta: CGPoint
    var previousPosition: CGPoint

    public init(node: SKNode, duration: TimeInterval, startPosition: CGPoint, endPosition: CGPoint) {
        previousPosition = node.position
        self.startPosition = startPosition
        delta = endPosition - startPosition
        super.init(node: node, duration: duration)
    }

    public override func update(t: CGFloat) {
        // Esto permite que múltiples objetos SKTMoveEffect modifiquen el mismo nodo
        // al mismo tiempo.
        let newPosition = startPosition + delta * t
        let diff = newPosition - previousPosition
        previousPosition = newPosition
        node.position += diff
    }
}

/**
 * Escala un nodo a un factor de escala determinado.
 */
public class SKTScaleEffect: SKTEffect {
    var startScale: CGPoint
    var delta: CGPoint
    var previousScale: CGPoint

    public init(node: SKNode, duration: TimeInterval, startScale: CGPoint, endScale: CGPoint) {
        previousScale = CGPoint(x: node.xScale, y: node.yScale)
        self.startScale = startScale
        delta = endScale - startScale
        super.init(node: node, duration: duration)
    }

    public override func update(t: CGFloat) {
        let newScale = startScale + delta * t
        let diff = newScale / previousScale
        previousScale = newScale
        node.xScale *= diff.x
        node.yScale *= diff.y
    }
}

/**
 * Rota un nodo a un cierto ángulo.
 */
public class SKTRotateEffect: SKTEffect {
    var startAngle: CGFloat
    var delta: CGFloat
    var previousAngle: CGFloat

    public init(node: SKNode, duration: TimeInterval, startAngle: CGFloat, endAngle: CGFloat) {
        previousAngle = node.zRotation
        self.startAngle = startAngle
        delta = endAngle - startAngle
        super.init(node: node, duration: duration)
    }

    public override func update(t: CGFloat) {
        let newAngle = startAngle + delta * t
        let diff = newAngle - previousAngle
        previousAngle = newAngle
        node.zRotation += diff
    }
}

/**
 * Envoltorio que te permite usar objetos SKTEffect como SKActions regulares.
 */
public extension SKAction {
    class func actionWithEffect(effect: SKTEffect) -> SKAction {
        return SKAction.customAction(withDuration: effect.duration) { node, elapsedTime in
            var t = elapsedTime / CGFloat(effect.duration)

            if let timingFunction = effect.timingFunction {
                t = timingFunction(t)  // aquí ocurre la magia
            }

            effect.update(t: t)
        }
    }
}

